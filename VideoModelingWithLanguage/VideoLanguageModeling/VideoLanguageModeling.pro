QT += core
QT -= gui

CONFIG += c++11

TARGET = VideoLanguageModeling
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    videosemantics.cpp \
    processengine.cpp \
    opticalflow.cpp

HEADERS += \
    videosemantics.h \
    processengine.h \
    opticalflow.h

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += opencv
