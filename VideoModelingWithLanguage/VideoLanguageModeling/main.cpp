#include <QCoreApplication>
#include "processengine.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv); // Creating a event loop which uses the console - > it has a signal called 'AboutToQuit' and slot called 'quit'
    ProcessEngine pr(argc,argv);

    // connection to slot
    QObject::connect(&pr,&ProcessEngine::processingDone, &app, &QCoreApplication::quit,Qt::QueuedConnection);

    // process the data
    pr.displayData();

    return app.exec(); // calling the 'app' event loop. it will not exit untill 'quit' slot is called.
}
