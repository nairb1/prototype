#include "opticalflow.h"

OpticalFlow::OpticalFlow(QObject *parent) : QObject(parent)
{
}

void OpticalFlow::drawOpticalFlow(const Mat_<float> &flowx, const Mat_<float> &flowy, Mat &dst, float maxmotion)
{
    dst.create(flowx.size(), CV_8UC3);
    dst.setTo(Scalar::all(0));

    // determine motion range:
    float maxrad = maxmotion;

    if (maxmotion <= 0)
    {
        maxrad = 1;
        for (int y = 0; y < flowx.rows; ++y)
        {
            for (int x = 0; x < flowx.cols; ++x)
            {
                Point2f u(flowx(y, x), flowy(y, x));

                if (!isFlowCorrect(u))
                    continue;

                maxrad = max(maxrad, sqrt(u.x * u.x + u.y * u.y));
            }
        }
    }

    for (int y = 0; y < flowx.rows; ++y)
    {
        for (int x = 0; x < flowx.cols; ++x)
        {
            Point2f u(flowx(y, x), flowy(y, x));

            if (isFlowCorrect(u))
                dst.at<Vec3b>(y, x) = computeColor(u.x / maxrad, u.y / maxrad);
        }
    }
}

void OpticalFlow::processImages(const Mat img1, const Mat img2, Mat &dst)
{
    cv::cuda::GpuMat g_frame0, g_frame1, g_frame0f, g_frame1f;
    std::vector<cv::cuda::GpuMat> g_planes;
    g_frame0.upload(img1);
    g_frame1.upload(img2);

    g_frame0.convertTo(g_frame0f, CV_32F, 1.0 / 255.0);
    g_frame1.convertTo(g_frame1f, CV_32F, 1.0 / 255.0);

    cv::cuda::GpuMat d_flow(img1.size(),CV_32FC2);

    //Ptr<cv::cuda::OpticalFlowDual_TVL1> fl_method = cv::cuda::OpticalFlowDual_TVL1::create();
    Ptr<cuda::BroxOpticalFlow> fl_method = cuda::BroxOpticalFlow::create(0.197f, 50.0f, 0.8f, 10, 77, 10);

    fl_method->calc(g_frame0f,g_frame1f,d_flow);

    cv::cuda::split(d_flow,g_planes);

    cv::Mat flowx(g_planes[0]);
    cv::Mat flowy(g_planes[1]);

    cv::Mat out;
    drawOpticalFlow(flowx, flowy, dst, 10);

    // just downloading the images from the GPU
    Mat frame0, frame1;
    g_frame0.download(frame0);
    g_frame1.download(frame1);

    return;
}
