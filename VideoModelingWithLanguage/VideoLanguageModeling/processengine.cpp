#include "processengine.h"

ProcessEngine::ProcessEngine(int argc, char** argv, QObject *parent) : QObject(parent)
{
    // Initialization of analytics structures
    v_sem = new VideoSemantics(argv[1]);
}

void ProcessEngine::displayData()
{
    //v_sem->playVideo();
    v_sem->motionSegmentation(5);

    // after processing is done
    emit processingDone();

}
