#ifndef PROCESSENGINE_H
#define PROCESSENGINE_H

#include <QObject>
#include <QPointer>
#include "videosemantics.h"

class ProcessEngine : public QObject
{
    Q_OBJECT
public:
    explicit ProcessEngine(int argc, char** argv, QObject *parent = 0);
    void displayData();

signals:
    void processingDone();

public slots:

private:
  QPointer<VideoSemantics> v_sem;
};

#endif // PROCESSENGINE_H
