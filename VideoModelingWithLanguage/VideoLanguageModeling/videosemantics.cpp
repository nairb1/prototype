#include "videosemantics.h"

VideoSemantics::VideoSemantics(QString videoFileName,QObject *parent) : QObject(parent)
{
    // Initialize Optical FLow


    // initialize capture of video
    capture.open(videoFileName.toStdString());
}

void VideoSemantics::playVideo()
{
    cv::Mat frame,frame_gray;

    while(1){
        capture >> frame;

        if(frame.empty())
            break;
        cv::imshow(INPUT_WIN,frame);

        char c = waitKey(30);
        if(c == 'q')
            break;
    }

    this->capture.release();
    destroyAllWindows();
}

void VideoSemantics::motionSegmentation(int frame_skip)
{
    int gpu_count = cuda::getCudaEnabledDeviceCount();
    if(gpu_count > 0){
        // Call GPU based Optical Flow

        cv::Mat frame,prev_frame, flow_frame, frame_gray;
        cv::namedWindow(INPUT_WIN);
        cv::namedWindow(OUTPUT_WIN);
        int count = 0;
        while(1){
            for(int sk = 0; sk <= frame_skip; sk++)
                capture >> frame;
            if(frame.empty())
                break;
            cv::cvtColor(frame,frame_gray,COLOR_RGB2GRAY);
            cv::imshow(INPUT_WIN,frame);
            count++;
            if(count > 10){
                OpticalFlow::processImages(prev_frame,frame_gray,flow_frame);
                cv::imshow(OUTPUT_WIN,flow_frame);
            }

            char c = waitKey(30);
            if(c == 'q')
                break;

            prev_frame = frame_gray.clone();
        }

        this->capture.release();
        destroyAllWindows();
    }
    else{
        // No GPU- Provide a message
        std::cout << "No GPU Present" << std::endl;
    }
}
