#ifndef VIDEOSEMANTICS_H
#define VIDEOSEMANTICS_H

#include <QObject>
#include <QString>
#include <QPointer>

#include <opencv.hpp>
#include "opticalflow.h"
#include "imgproc.hpp"
#include "cudaimgproc.hpp"
#include "cudafilters.hpp"

using namespace cv;

#define INPUT_WIN "Input Video"
#define OUTPUT_WIN "Output Video"

class VideoSemantics : public QObject
{
    Q_OBJECT
protected:
    cv::VideoCapture capture;
    QString videoFileName;

public:
    explicit VideoSemantics(QString videoFileName,QObject *parent = 0);

    void playVideo();
    void motionSegmentation(int frame_skip = 0);

signals:

public slots:

};

#endif // VIDEOSEMANTICS_H
