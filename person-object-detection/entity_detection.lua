-- Main file which reads an image, entities various regions within the image
-- And classifies entities regions as various objects.
require 'torch'
require 'cutorch'
require 'cudnn'
require 'cunn'
require 'cunnx'
require 'nn'
require 'dp'

-- command line arguments
local cmd = torch.CmdLine()

cmd:text("Entity detection module")
cmd:text("")

cmd:option('-classification_model','pretrained_models/resnet-18.t7','model used to classify entitiesd entities')
cmd:option('-entitiestion_model','pretrained_models/deepmask/model.t7','model used for entitiestion')
cmd:option('-input_image_location','data/image.png','test image to find entities')
cmd:option('-output_image_location','data/output_image.png','output image which shows entities and labels')

-- entitiestion model parameters
cmd:option('-model','pretrained/deepmask','path to model to load')
cmd:option('-gpu', 1, 'gpu device')
cmd:option('-np', 10,'number of proposals to save in test')
cmd:option('-si', -2.5, 'initial scale')
cmd:option('-sf', .5, 'final scale')
cmd:option('-ss', .5, 'scale step')
cmd:option('-dm', false, 'use DeepMask version of SharpMask')
cmd:option('-prune_factor',0.5,'factor used in removing unwanted detections')

local config = cmd:parse(arg or{})

-- various initializations
torch.setdefaulttensortype('torch.FloatTensor')
cutorch.setDevice(config.gpu)

local coco = require 'coco'
local maskApi = coco.MaskApi

local meanstd = {mean = { 0.485, 0.456, 0.406 }, std = { 0.229, 0.224, 0.225 }}

-- load deep mask model
paths.dofile('DeepMask.lua')
paths.dofile('SharpMask.lua')

print('| loading model file... ' .. config.entitiestion_model)
local m = torch.load(config.entitiestion_model)
local model = m.model
model:inference(config.np)
model:cuda()

-- create inference module
local scales = {}
for i = config.si,config.sf,config.ss do table.insert(scales,2^i) end

if torch.type(model)=='nn.DeepMask' then
  paths.dofile('InferDeepMask.lua')
elseif torch.type(model)=='nn.SharpMask' then
  paths.dofile('InferSharpMask.lua')
end

local infer = Infer{
  np = config.np,
  scales = scales,
  meanstd = meanstd,
  model = model,
  dm = config.dm,
}

-- load the classification model
local resnet_model = torch.load(config.classification_model)
resnet_model:add(cudnn.SoftMax():cuda())
resnet_model:evaluate() -- set in evaluate mode

-- The model was trained with this input normalization
local meanstd = {
   mean = { 0.485, 0.456, 0.406 },
   std = { 0.229, 0.224, 0.225 },
}

local transform = t.Compose{
   t.Scale(256),
   t.ColorNormalize(meanstd),
   t.CenterCrop(224),
}

-- load image
local img = image.load(config.image_file)

-- segments the image and gets entities
entities = entity_extract(temp_img)

-- classifies those entities






-- function to extract entities
local function entity_extract(temp_img)
  -- Apply deep mask
  infer:forward(temp_img)

  -- obtain the masks and scores
  local masks,scores = infer:getTopProps(.2,h,w)
  local res = temp_img:clone()

  -- filter the masks based on score
  local val_count = 0
  for k=1,config.np,1 do
    score_per_mask = scores[k][1]
    if score_per_mask > config.prune_factor then
      val_count = val_count + 1
    end
  end

  local entities = {}
  if val_count > 0 then
    -- encode the mask
    masks_rs = maskApi.encode(masks_new)
    -- get the bounding boxes
    bbs = maskApi.toBbox(masks_rs)

    -- Iterate through each bounding box
    for k = 1,bbs:size(1),1 do
      -- Applying method 1 ( refer extract_glimpses_all_images.lua)
      x = bbs[k][1] ; y = bbs[k][2] ; w = bbs[k][3] ; h = bbs[k][4]

      -- get the entities
      local img_seg = image.crop(temp_img,x,y,x+w,y+h)
      entities[k] = img_seg
    end
  end

  return entities


end

-- function to classify entities
local function entity_classify(entity_imgs)

  -- transform the table of entities to tensor
  img_batch = torch.cat(entity_imgs,1)
  num_batches = img_batch:size(1)

  -- normalize the images
  for i=1,num_batches,1 do
    img_batch[i] = transform(img_batch[i])
  end

  -- apply the output
  local output = resnet_model:forward(img_batch)

-- TODO: Need to replace this with single class, and appropriate index
-- TODO: Check the appropriate set of labels, and retrain the dataset
-- TODO: Use fine-tune mechanism in Facebook resNet to get re-trained model for those classes
-- TODO: The important classes are person, any kind of cars, from ImageNET and COCO databases
-- TODO: we can use the same fine-tune code by facebook ResNet, but however, we will need to fine-tune the dataset with
-- TODO: custom labels, the images of those labels need to be re-arranged.

--  -- Get the top 5 class indexes and probabilities
--  local probs, indexes = output:topk(N, true, true)
--  print('Classes for', arg[i])
--  for n=1,N do
--  print(probs[n], imagenetLabel[indexes[n]])
--  print('')

end




